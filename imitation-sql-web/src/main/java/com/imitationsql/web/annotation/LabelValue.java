package com.imitationsql.web.annotation;

import java.lang.annotation.*;

/**
 * <p>Description: some description </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 16:03
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LabelValue {

    /**
     * label
     *
     * @return
     */
    String label();

    /**
     * value
     *
     * @return
     */
    String value();
}
