package com.imitationsql.web.annotation;

import com.imitationsql.core.enums.OperateEnum;
import com.imitationsql.web.enums.ColumnTypeEnum;

import java.lang.annotation.*;

/**
 * <p>Description: 列注解，前端页面展示的字段 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 13:55
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Column {

    /**
     * 是否可以编辑
     *
     * @return
     */
    boolean enableEdit() default false;

    /**
     * 列类型
     *
     * @return
     */
    ColumnTypeEnum type() default ColumnTypeEnum.INPUT;


    /**
     * 多选label  value
     *
     * @return
     */
    LabelValue[] mulitValue() default {};

    /**
     * 是否是查询条件
     *
     * @return
     */
    boolean isQueryParam() default false;

    /**
     * 操作，默认 eq查询
     *
     * @return
     */
    OperateEnum queryType() default OperateEnum.EQ;

    /**
     * 是否校验
     *
     * @return
     */
    boolean isValidate() default false;

    /**
     * 校验规则正则表达式
     *
     * @return
     */
    String validateReg() default "";

    /**
     * 是否动态加载数据
     *
     * @return
     */
    boolean isDynamicLoading() default false;

    /**
     * 动态加载配置
     *
     * @return
     */
    DynamicDataMeta dynamicData() default @DynamicDataMeta(url = "");

    /**
     * 排序
     *
     * @return
     */
    int sortOrder() default 0;
}
