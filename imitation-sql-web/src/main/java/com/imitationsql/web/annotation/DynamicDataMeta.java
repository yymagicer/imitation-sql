package com.imitationsql.web.annotation;

import java.lang.annotation.*;

/**
 * <p>Description: 动态数据 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 14:27
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DynamicDataMeta {

    /**
     * 请求接口地址
     *
     * @return
     */
    String url();

    /**
     * 方法，默认get
     *
     * @return
     */
    String method() default "get";

    /**
     * 数据结构
     *
     * @return
     */
    String dataStructure() default "{\"code\":\"\",\"msg\":\"\",\"data\":{}}";
}
