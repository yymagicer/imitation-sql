package com.imitationsql.web.annotation;

/**
 * <p>Description: 权限 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 15:06
 */
public @interface Power {

    /**
     * 新增
     *
     * @return
     */
    boolean add() default true;

    /**
     * 编辑
     *
     * @return
     */
    boolean edit() default true;

    /**
     * 删除
     *
     * @return
     */
    boolean delete() default true;

    /**
     * 批量删除
     *
     * @return
     */
    boolean batchDelete() default false;

    /**
     * 物理删除删除
     *
     * @return
     */
    boolean physicalBatchDelete() default false;

    /**
     * 查询
     *
     * @return
     */
    boolean query() default true;

    /**
     * 详情
     *
     * @return
     */
    boolean detail() default true;

    /**
     * 导出数据
     *
     * @return
     */
    boolean exportData() default false;

    /**
     * 导入数据
     *
     * @return
     */
    boolean importData() default false;
}
