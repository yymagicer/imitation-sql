package com.imitationsql.web.entity;

import com.imitationsql.core.annotation.Query;
import com.imitationsql.core.annotation.TableName;
import com.imitationsql.core.enums.OperateEnum;
import com.imitationsql.web.config.EnableAutoApi;
import com.imitationsql.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>Description: 菜单实体类 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/3/29 15:59
 */
@Setter
@Getter
@TableName("t_menu")
@EnableAutoApi
@Table(name = "t_menu")
@Entity
public class MenuEntity extends BaseEntity<Integer> {

    /**
     * 名称
     */
    @Query(operate = OperateEnum.LIKE)
    @javax.persistence.Column(name = "menu_name", columnDefinition = "varchar(255) not null COMMENT '名称'")
    private String menuName;

    /**
     * 实体类名称（后端使用）
     */
    @javax.persistence.Column(name = "entity_name", columnDefinition = "varchar(255)  COMMENT '实体类名称（后端使用）'")
    private String entityName;

    /**
     * 路由地址（前端使用）
     */
    @javax.persistence.Column(name = "route_url", columnDefinition = "varchar(255)  COMMENT '路由地址（前端使用）'")
    private String routeUrl;

    /**
     * 图标
     */
    @javax.persistence.Column(name = "icon", columnDefinition = "varchar(255)  COMMENT '图标'")
    private String icon;

    /**
     * 类型，0-目录;1-菜单;2-按钮
     */
    @javax.persistence.Column(name = "type", columnDefinition = "tinyint(2) not null DEFAULT '1'   COMMENT '类型，0-目录;1-菜单;2-按钮'")
    private Integer type;

    /**
     * 后端接口地址
     */
    @javax.persistence.Column(name = "api_url", columnDefinition = "varchar(255)  COMMENT '后端接口地址'")
    private String apiUrl;

    /**
     * 父类id
     */
    @javax.persistence.Column(name = "parent_id", columnDefinition = "int(11)  COMMENT '父类id'")
    private Serializable parentId;
}
