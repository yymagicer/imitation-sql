package com.imitationsql.web.entity;

import com.imitationsql.core.annotation.Query;
import com.imitationsql.core.annotation.TableName;
import com.imitationsql.core.enums.OperateEnum;
import com.imitationsql.web.config.EnableAutoApi;
import com.imitationsql.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Description: 角色实体类 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/3/19 16:49
 */
@Setter
@Getter
@TableName("t_role")
@EnableAutoApi
public class RoleEntity extends BaseEntity<Integer> {


    /**
     * 角色名称
     */
    @Query(operate = OperateEnum.LIKE)
    private String roleName;

    /**
     * 备注
     */
    private String remark;
}
