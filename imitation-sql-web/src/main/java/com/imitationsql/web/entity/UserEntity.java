package com.imitationsql.web.entity;

import com.imitationsql.core.annotation.Query;
import com.imitationsql.core.annotation.TableName;
import com.imitationsql.core.enums.OperateEnum;
import com.imitationsql.web.annotation.Column;
import com.imitationsql.web.config.EnableAutoApi;
import com.imitationsql.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Description: 用户实体类 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/3/12 16:53
 */
@Setter
@Getter
@TableName("t_user")
@EnableAutoApi
public class UserEntity extends BaseEntity<Integer> {

    /**
     * 名称
     */
    @Query(operate = OperateEnum.LIKE)
    @Column(enableEdit = true)
    @javax.persistence.Column(name = "user_name", columnDefinition = "varchar(255) not null COMMENT '名称'")
    private String userName;

    /**
     * 密码
     */
    @Column(enableEdit = true)
    @javax.persistence.Column(name = "password", columnDefinition = "varchar(255) not null COMMENT '密码'")
    private String password;

    /**
     * email
     */
    @Column(enableEdit = true)
    private String email;

    /**
     * 手机号
     */
    @Column(enableEdit = true)
    private String mobile;

    /**
     * 头像
     */
    @Column(enableEdit = true)
    private String avatar;

}
