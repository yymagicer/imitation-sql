package com.imitationsql.web.entity;

import com.imitationsql.core.annotation.TableName;
import com.imitationsql.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>Description: 角色菜单实体类 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/3/29 16:06
 */
@Setter
@Getter
@TableName("t_role_menu")
public class RoleMenuEntity extends BaseEntity<Integer> {

    /**
     * 角色id
     */
    private Serializable roleId;

    /**
     * 菜单id
     */
    private Serializable menuId;
}
