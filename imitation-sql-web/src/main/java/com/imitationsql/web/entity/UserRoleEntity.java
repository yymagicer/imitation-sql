package com.imitationsql.web.entity;

import com.imitationsql.core.annotation.TableName;
import com.imitationsql.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>Description: 用户角色实体类 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/3/29 15:58
 */
@Setter
@Getter
@TableName("t_user_role")
public class UserRoleEntity extends BaseEntity<Integer> {

    /**
     * 用户id
     */
    private Serializable userId;

    /**
     * 角色id
     */
    private Serializable roleId;
}
