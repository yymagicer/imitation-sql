package com.imitationsql.web.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * <p>Description: form表达元数据 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 15:10
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FormMetaData {

    /**
     * 列元数据
     */
    private List<ColumnMetaData> columnMetaDataList;

    /**
     * 权限
     */
    private List<String> powers;

}
