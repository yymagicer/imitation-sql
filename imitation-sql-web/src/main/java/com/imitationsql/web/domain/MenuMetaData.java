package com.imitationsql.web.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>Description: 菜单数据 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 17:35
 */
@Setter
@Getter
public class MenuMetaData {

    /**
     * 请求路径
     */
    private String path;

    /**
     * 组件
     */
    private String component;

    /**
     * 重定向
     */
    private String redirect;

    /**
     * 永远展示
     */
    private boolean alwaysShow;

    /**
     * 名称
     */
    private String name;

    /**
     * meta
     */
    private Meta meta;

    /**
     * 子类
     */
    private List<MenuMetaData> children;


    @Setter
    @Getter
    public static class Meta {

        /**
         * 标题
         */
        private String title;

        /**
         * icon
         */
        private String icon;

        /**
         * 角色
         */
        private List<String> roles;
    }
}
