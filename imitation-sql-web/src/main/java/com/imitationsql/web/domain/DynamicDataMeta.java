package com.imitationsql.web.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>Description: 动态数据元数据配置 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 14:25
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DynamicDataMeta {
    /**
     * 请求地址
     */
    private String url;

    /**
     * 方法；get ,post等
     */
    private String method;

    /**
     * 数据结构
     */
    private String dataStructure;
}
