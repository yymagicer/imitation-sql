package com.imitationsql.web.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>Description: 列元数据 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 15:25
 */
@Setter
@Getter
public class ColumnMetaData {

    /**
     * 名称
     */
    private String name;
    /**
     * 是否可以编辑
     */
    private boolean enableEdit;

    /**
     * 列类型
     *
     * @return
     */
    private String type;

    /**
     * 多选键值列表
     */
    private List<LabelValueMetaData> labelValues;

    /**
     * 是否是查询条件
     *
     * @return
     */
    private boolean isQueryParam;
    /**
     * 操作，默认 eq查询
     *
     * @return
     */
    private String queryType;


    /**
     * 是否校验
     *
     * @return
     */
    private boolean isValidate;

    /**
     * 校验规则正则表达式
     *
     * @return
     */
    private String validateReg;

    /**
     * 是否动态加载数据
     *
     * @return
     */
    private boolean isDynamicLoading;

    /**
     * 动态加载配置
     *
     * @return
     */
    private DynamicDataMeta dynamicData;
}
