package com.imitationsql.web.domain;

import com.imitationsql.core.annotation.PrimaryKey;
import com.imitationsql.core.constants.BaseEntityConstant;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>Description: some description </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/3/15 13:59
 */
@Setter
@Getter
@MappedSuperclass
public class BaseEntity<T extends Serializable> {
    /**
     * id
     */
    @PrimaryKey
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11) COMMENT '自增长ID'")
    private T id;

    /**
     * 是否删除
     */
    @Column(name = "deleted", columnDefinition = "tinyint(1) not null DEFAULT '0' COMMENT '配置名称'")
    private Integer deleted = BaseEntityConstant.NOT_DELETE;

    /**
     * 创建人id
     */
    @Column(name = "create_user_id", columnDefinition = "int(11)  COMMENT '创建人id'")
    private T createUserId;

    /**
     * 创建时间
     */
    @Column(name = "create_time", columnDefinition = "datetime COMMENT '创建时间'")
    private Date createTime;

    /**
     * 更新人id
     */
    @Column(name = "update_user_id", columnDefinition = "int(11)  COMMENT '更新人id'")
    private T updateUserId;

    /**
     * 更新时间
     */
    @Column(name = "update_time", columnDefinition = "datetime COMMENT '更新时间'")
    private Date updateTime;

}
