package com.imitationsql.web.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>Description: some description </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 16:06
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LabelValueMetaData {

    /**
     * label
     */
    private String label;

    /**
     * value
     */
    private String value;
}
