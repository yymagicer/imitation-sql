package com.imitationsql.web.service;

import com.imitationsql.web.domain.MenuMetaData;

import java.util.List;

/**
 * <p>Description: some description </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 17:41
 */
public interface MenuService {

    /**
     * 树形结构
     *
     * @return
     */
    List<MenuMetaData> tree();
}
