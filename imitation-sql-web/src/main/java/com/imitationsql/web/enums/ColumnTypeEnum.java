package com.imitationsql.web.enums;

import lombok.Getter;

/**
 * <p>Description: 列的类型 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 14:13
 */
@Getter
public enum ColumnTypeEnum {
    /**
     * 输入框
     */
    INPUT("input", "输入框"),
    /**
     * 单选框
     */
    RADIO("radio", "单选框"),
    /**
     * 多选框
     */
    CHECKBOX("checkbox", "多选框"),
    /**
     * 选择器
     */
    SELECT("select", "选择器"),
    /**
     * 开关
     */
    SWITCH("switch", "开关"),
    /**
     * 日期时间选择器
     */
    DATE_TIME_PICKER("dateTimePicker ", "日期时间选择器"),
    ;
    /**
     * 类型
     */
    private final String type;
    /**
     * 描述
     */
    private final String desc;

    ColumnTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
