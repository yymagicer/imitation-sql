package com.imitationsql.web.tree;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>Description: 闭包表 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/8 15:46
 */
@Setter
@Getter
public class ClosureTable {

    /**
     * id
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 父类id
     */
    private String parentId;

    /**
     * 深度
     */
    private Integer depth;

    /**
     * 子类
     */
    private List<? extends ClosureTable> children;
}
