package com.imitationsql.web.api;

import com.imitationsql.web.domain.CommonResult;
import com.imitationsql.web.domain.MenuMetaData;
import com.imitationsql.web.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>Description: 菜单接口 </p>
 *
 * @author : xiaodong.yang
 * @date : 2024/4/12 17:40
 */
@RestController
@RequestMapping("/menu")
public class MenuController {


    @Autowired
    private MenuService menuService;

    /**
     * 获取树形结构
     *
     * @return
     */
    @GetMapping("/tree")
    public CommonResult<List<MenuMetaData>> tree() {
        return CommonResult.ok(menuService.tree());
    }
}
